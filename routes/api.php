<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('property', 'PropertyController@index');
    $router->post('property', 'PropertyController@store');
    $router->put('property/{id}', 'PropertyController@update');
    $router->delete('property/{id}', 'PropertyController@destroy');

    $router->get('search-profile', 'SearchProfileController@index');
    $router->post('search-profile', 'SearchProfileController@store');
    $router->put('search-profile/{id}', 'SearchProfileController@update');
    $router->delete('search-profile/{id}', 'SearchProfileController@destroy');

    $router->get('/matcher', 'MatcherController@index');
    $router->post('/calculate-score', 'MatcherController@matchAndInsert');
    $router->get('/match/{id}', 'MatcherController@match');
});