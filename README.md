# Matcher Microservice Case Study (Lumen + Vuejs)
***Developed By Syed Muhammad Ali Kamal***

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]()

# Installation Guide

#### Requirements
- PHP >= 7.4
- Lumen >= 8.x (Please see the full lumen installation guide & system requiements [here][lumen])
- VueJS 2.x    (Please see the full lumen installation guide & system requiements [here][vuejs])
- MongoDB >= 3.x.x
 

#### Project Installation
##### STEP 1:
Download the project source code from bitbucket [here][sourcecode]
##### STEP 2:
Goto the project directory and delete following files/directories:
1) Vendor
2) node_modules
3) composer.lock
4) package-lock.json

After deleting these files/directories run folowing commands on project's root directory:
```sh
composer install
npm install
npm run watch
```
> Please Note: You have to run your `Local server` in order to run PHP and serve Lumen.

To serve your project locally, you may use the [Laravel Homestead](https://laravel.com/docs/9.x/homestead) virtual machine, [Laravel Valet](https://laravel.com/docs/9.x/valet), or the built-in PHP development server:
```sh
php -S localhost:8000 -t public
```

   [lumen]: <https://lumen.laravel.com/docs/8.x>
   [vuejs]: <https://v2.vuejs.org/v2/guide/installation.html?redirect=true>
   [sourcecode]: <https://bitbucket.org/goodayz/matcher-microservice>
   
