<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Matcher | Microservice Case Study</title>
    <link rel="shortcut icon" type="image/x-icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Bitbucket-blue-logomark-only.svg/1200px-Bitbucket-blue-logomark-only.svg.png"/>
    <link rel="stylesheet" href="css/app.css" />
  </head>
  <body>
    <div id="app">
      <main-header></main-header>
      <router-view></router-view>
    </div>
    <script src="js/app.js"></script>
  </body>
</html>