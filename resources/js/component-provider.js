import Vue from 'vue';

Vue.component('main-header', require('./components/Header.vue'));
Vue.component('left-menu', require('./components/LeftMenu.vue'));
Vue.component('vue-pagination', require('./components/VuePagination.vue'));
Vue.component('common-modal', require('./components/CommonModal.vue'));
Vue.component('async-select-option', require('./components/AsyncSelectOption'));