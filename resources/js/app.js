import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router';
import routes from './routes'
import vPagination from 'vue-plain-pagination'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue' 

// Import Bootstrap an BootstrapVue CSS files (order is important)
// import '../../node_modules/bootstrap/dist/css/bootstrap.css'
import '../../node_modules/bootstrap-vue/dist/bootstrap-vue.css'

require('./component-provider');

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.use(vPagination);

axios.defaults.baseURL = process.env.MIX_API_URL

const router = new VueRouter({
    mode: 'history',
    routes: routes
})

const app = new Vue({
  router
}).$mount('#app')
