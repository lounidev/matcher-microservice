const routes = [
    {
        name: 'property',
        path: '/',
        meta: {
        },
        component: require('../pages/property-page.vue'),
    },
    {
        name: 'search-profiles',
        path: '/search-profiles',
        meta: {
        },
        component: require('../pages/search-profile-page.vue'),
    },
    {
        name: 'matcher',
        path: '/matcher',
        meta: {
        },
        component: require('../pages/matcher-page.vue'),
    },
]

export default routes