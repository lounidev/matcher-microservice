<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SearchProfile;
use Carbon\Carbon;

class SearchProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    protected function tenRecords()
    {
        $price = [
            ['500000', '6500000'],
            ['600000', NULL],
            ['1000000', '1300000'],
            ['600000', '700000'],
            [ NULL, '6500000'],
            ['800000', '1000000'],
            ['100000', '300000'],
            ['200000', '300000'],
            [ NULL, '100000'],
            ['800000', NULL],
            ['500000', '6500000']
        ];

        $area = [
            ['40', '80'],
            ['100', NULL],
            ['60', '100'],
            ['120', '140'],
            [ NULL, '120'],
            ['60', '80'],
            ['80', '120'],
            ['120', '240'],
            [ NULL, '240'],
            ['40', NULL],
            ['60', '100']
        ];
        
        $yearOfConstruction = [
            ['2010', '2012'],
            ['2011', NULL],
            ['2015', '2017'],
            ['2010', '2015'],
            [ NULL, '2020'],
            ['2014', '2016'],
            ['80', '120'],
            ['2018', '2020'],
            [ NULL, '2019'],
            ['2021', NULL],
            ['2011', '2012']
        ];

        $rooms = [
            ['3', '4'],
            ['2', NULL],
            ['1', '3'],
            ['3', '5'],
            [ NULL, '4'],
            ['4', '5'],
            ['2', '3'],
            ['4', '5'],
            [ NULL, '3'],
            ['1', NULL],
            ['3', '5']
        ];
        $parking = [true,true,true,false,true,false,true,true,true,true,true];

        $returnActual = [
            ['13', '14'],
            ['12', NULL],
            ['10.5', '13'],
            ['13', '15'],
            [ NULL, '14'],
            ['14.2', '15'],
            ['12.5', '13'],
            ['10', '15'],
            [ NULL, '12'],
            ['11', NULL],
            ['13', '15']
        ];

        $size = 10;
        $datetime = Carbon::now()->toDateTimeString();
        for($j = 0; $j < 10; $j++) {
            $data = [];
            for($i = 0; $i < $size; $i++) {
                $data[] = [
                    "name" => "Customer " . (($j * 10) + ($i + 1) ),
                    "propertyType" => $j + 1,
                    "searchFields" => [
                        "price" => $price[$i], 
                        "area" => $area[$i], 
                        "yearOfConstruction" => $yearOfConstruction[$i],
                        "rooms" => $rooms[$i],
                        "parking" => $parking[$i],
                    ], 
                    "returnPotential" => $returnActual[$i],
                    'created_at' => $datetime,
                    'updated_at' => $datetime
                ];
            }
            SearchProfile::insert($data);
        }

    }

    protected function millionRecords($size = 30000)
    {
        ini_set('memory_limit', '512M');
        $datetime = Carbon::now()->toDateTimeString();
        for($j = 0; $j < 10; $j++) {
            $data = [];
            for($i = 0; $i < $size; $i++) {
                $p = rand(400000, 1500000);
                $price = [$p, rand($p, 1500000)];
                $a = rand(40, 240);
                $area = [$a, rand($a, 240)];
                $a = rand(2010, 2022);
                $yearOfConstruction = [$a, rand($a, 2022)];
                $a = rand(1,5);
                $rooms = [$a, 5];

                $data[] = [
                    "name" => "Customer " . (($j * 10) + ($i + 1) ),
                    "propertyType" => $j + 1,
                    "searchFields" => [
                        "price" => $price, 
                        "area" => $area, 
                        "yearOfConstruction" => $yearOfConstruction,
                        "rooms" => $rooms,
                        "parking" => rand(0, 1) == 1? true: false,
                    ], 
                    'created_at' => $datetime,
                    'updated_at' => $datetime,
                    "returnPotential" => rand(10, 20)
                ];
            }
            SearchProfile::insert($data);
        }
        
    }

    public function run()
    {
        SearchProfile::truncate();
        $this->tenRecords();
        // $this->millionRecords();
    }
}
