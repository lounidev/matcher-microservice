let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'js/');
mix.sass('resources/scss/app.scss', 'css/');