<?php

namespace App\Traits;

/**
 * @author Syed Muhammad Ali Kamal
 */
trait HasSortable
{
    /**
     * Sort the data based on attribute and sort flag.
     * 
     * @param  string  $key
     * @param  string  $order
     * @return $this
     */
    public function orderBy($key, $order = 'DESC')
    {
        $data = $this->get();
        usort($data, function($a, $b) use ($key, $order) {
            return ($order === 'DESC' || $order === 'desc')? $a[$key] < $b[$key]: $a[$key] > $b[$key];
        });
        $this->setResult($data);
        return $this;
    }
}