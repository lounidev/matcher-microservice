<?php

namespace App\Traits;

/**
 * @author Syed Muhammad Ali Kamal
 */
trait HasSerializable
{
    /**
     * iterates over each record and call a callback function.
     * 
     * @param function $fn
     * @return bool
     */
    public function serialize($fn)
    {
        $records = [];
        foreach($this->getSearchData() as $data) {
            $records[] = $fn($data);
        }
        $this->setSearchData($records);
        return $this;
    }
}