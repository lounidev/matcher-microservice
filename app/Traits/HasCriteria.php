<?php

namespace App\Traits;

/**
 * @author Syed Muhammad Ali Kamal
 */
trait HasCriteria
{
    /**
     * Perform match based on range value.
     * 
     * @param  array    $field
     * @param  array    $range [min, max]
     * @param  integer  $deviation
     * @return bool
     */
    public function arrayCriteria($field, $range, $deviation = 0) {

        if(is_array($range)) {
            [$min, $max] = $range;
            $min = $min === NULL? 0: $min;
            $max = $max === NULL? $field: $max;
            $range = [$min, $max];
            [$min, $max] = $this->deviation($range, $this->getDeviation());
            if( (($min === NULL) || ($min !== NULL && $min <= $field)) 
                &&
                (($max === NULL) || ($max !== NULL && $max >= $field))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Perform match based on numeric value.
     * 
     * @param  array  $field range [min, max]
     * @param  array  $record
     * @return bool
     */
    public function numericCriteria($field, $value) {
        return is_numeric($value)? ($field === $value): false;
    }

    /**
     * Perform match based on boolean value.
     * 
     * @param  array  $field range [min, max]
     * @param  array  $record
     * @return bool
     */
    public function booleanCriteria($field, $value) {
        return is_bool($value)? ($field === $value): false;
    }

    /**
     * Deviation formula for integer and array value.
     * 
     * @param  integer|array  $value
     * @param  integer        $deviation
     * @return integer|array  $value
     */
    function deviation($value, $deviation = 0) 
    { 
        $deviation = $deviation / 100;
        if(is_array($value)) {
            $value[0] = $value[0] - ($value[0] * $deviation);
            $value[1] = $value[1] + ($value[1] * $deviation);
        }else {
            $value = $value + ($value * $deviation);
        }
        return $value;
    } 

    /**
     * Iterates over each record and perform the match criteria.
     * 
     * @param  string                     $key
     * @param  string|integer|bool|array  $field
     * @param  array                      $record
     * @param  integer                    $deviation
     * @return bool
     */
    public function matchCriteria($key, $field, $record, $deviation = 0) {
        $foundMatch = false;
        if(isset($record[$key])) {
            $value = $record[$key];
            if($foundMatch = $this->arrayCriteria($field, $value, $deviation)) {} 
            else if($foundMatch = $this->numericCriteria($field, $value)) {}
            else if($foundMatch = $this->booleanCriteria($field, $value)) {}
        }
        return $foundMatch;
    }
}