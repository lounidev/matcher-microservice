<?php

namespace App\Services;

use App\Services\Matcher;
use App\Contracts\CriteriaFactoryContract;
use App\Contracts\Responsable;

/**
 * @author Syed Muhammad Ali Kamal
 */
class CustomerProfileMatcher extends Matcher implements Responsable
{
    /**
     * Aliases of filters.
     *
     * @var array
     */
    protected $aliases = [
        'returnActual' => 'returnPotential'
    ];

    /**
     * Create a new CustomerProfileMatcher instance.
     *
     * @param  CriteriaFactoryContract $factory
     * @param  array $params
     * @return void
     */
    public function __construct(CriteriaFactoryContract $factory)
    {
        parent::__construct($factory);
    }

    /**
     * Tranform the data before passing it to the matching algorithm.
     * 
     * @param  array $data
     * @return array
     */
    public function transformData($data)
    {
        if(isset($data['returnPotential'])) {
            $data['searchFields']['returnPotential'] = $data['returnPotential'];
            unset($data['returnPotential']);
        }
        return $data;
    }
    
    /**
     * Convert the result into specific format.
     * 
     * @param array $response
     * @param array $matchedCriteria
     * @return $data
     */
    public function formattedResponse($data, $matchedCriteria)
    {
        $strictMatchesCount = 0;
        $looseMatchesCount = 0;
        foreach($matchedCriteria as $criteria) {
            if($criteria['is_matched'] === true) {
                $strictMatchesCount++;
            }else if(is_array($criteria['is_matched'])) {
                if($criteria['is_matched']['match_found'] === true && $criteria['is_matched']['filter_applied'] === true) {
                    $looseMatchesCount++;
                }else if($criteria['is_matched']['match_found'] === true && $criteria['is_matched']['filter_applied'] === false) {
                    $strictMatchesCount++;
                }
            }
        }
        return [
            'searchProfileId'    => $data['_id'],
            'score'              => $strictMatchesCount + ($looseMatchesCount * 0.5),
            'strictMatchesCount' => $strictMatchesCount,
            'looseMatchesCount'  => $looseMatchesCount
        ];
    }
}
