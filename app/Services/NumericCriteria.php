<?php

namespace App\Services;

use App\Contracts\CriteriaContract;

/**
 * @author Syed Muhammad Ali Kamal
 */
class NumericCriteria implements CriteriaContract
{
    /**
     * The criteria value which is used in matching.
     *
     * @var array
     */
    protected $criteria_value = [];

    /**
     * Create a new CriteriaFactory instance.
     *
     * @return void
     */
    public function __construct($criteria_value)
    {
        $this->criteria_value = $criteria_value;
    }

    /**
     * Set the criteria value.
     *
     * @param mixed $criteria_value
     * @return $this
     */
    public function setCriteriaValue($criteria_value)
    {
        $this->criteria_value = $criteria_value;
        return $this;
    }

    /**
     * Retrieve the criteria value.
     *
     * @return mixed
     */
    public function getCriteriaValue()
    {
        return $this->criteria_value;
    }
    
    /**
     * Checks whether this criteria is applicable for criteria value 
     * by returning true / false.
     * 
     * @param  mixed $value
     * @return bool
     */
    public function applicable(): bool {
        return is_numeric($this->getCriteriaValue());
    }
    
    /**
     * Perform match based on range value.
     * 
     * @param  string|integer $value
     * @return bool
     */
    public function criteria($value): bool {
        return $this->getCriteriaValue() === $value;
    }

    /**
     * Alters the value before applying criteria.
     *
     * @param array $alter
     * @return $this
     */
    public function alter(Array $alter = [])
    {   
        return $this;
    }
    
}
