<?php

namespace App\Services;

use App\Contracts\CriteriaContract;

/**
 * @author Syed Muhammad Ali Kamal
 */
class ArrayWithDeviationCriteria implements CriteriaContract
{
    /**
     * The criteria value which is used in matching.
     *
     * @var array
     */
    protected $criteria_value = [];

    /**
     * Create a new CriteriaFactory instance.
     *
     * @return void
     */
    public function __construct($criteria_value)
    {
        $this->criteria_value = $criteria_value;
    }

    /**
     * Set the criteria value.
     *
     * @param mixed $criteria_value
     * @return $this
     */
    public function setCriteriaValue($criteria_value)
    {
        $this->criteria_value = $criteria_value;
        return $this;
    }

    /**
     * Retrieve the criteria value.
     *
     * @return mixed
     */
    public function getCriteriaValue()
    {
        return $this->criteria_value;
    }
    
    /**
     * Checks whether this criteria is applicable for criteria value 
     * by returning true / false.
     * 
     * @param  mixed $value
     * @return bool
     */
    public function applicable(): bool {
        return is_array($this->getCriteriaValue());
    }
    

    /**
     * Perform match based on range value.
     * 
     * @param  string|integer $value
     * @param  array          $details
     * @return bool
     */
    public function criteria($value, $details = []) {

        [$min, $max] = $this->criteria_value;
        $min = $min === NULL? 0: $min;
        $max = $max === NULL? $value: $max;

        if($this->isBetween($value, $min, $max)) {
            return [
                'match_found' => true,
                'filter_applied' => false,
            ];
        }else if(isset($details['deviation'])) {

            $deviation = $details['deviation'] / 100;
            $min = $min - ($min * $deviation);
            $max = $max + ($max * $deviation);

            if($this->isBetween($value, $min, $max)) {
                return [
                    'match_found' => true,
                    'filter_applied' => true,
                ];
            }
        }
        return false;
    }

    private function isBetween($value, $min, $max) {
        if( (($min === NULL) || ($min !== NULL && $min <= $value)) 
        &&
        (($max === NULL) || ($max !== NULL && $max >= $value))) {
            return true;
        }
        return false;
    }

}
