<?php

namespace App\Services;

use App\Contracts\CriteriaFactoryContract;
use App\Contracts\MatcherContract;
use App\Contracts\Serializable;
use App\Contracts\Responsable;
use App\Contracts\Sortable;
use App\Traits\HasSerializable;
use App\Traits\HasSortable;

/**
 * @author Syed Muhammad Ali Kamal
 */
class Matcher implements MatcherContract, Serializable, Responsable, Sortable
{
    use HasSerializable, HasSortable;

    /**
     * The filter that is used to match data.
     *
     * @var array
     */
    protected $filters = [];

    /**
     * The search data to perform filteration.
     *
     * @var array
     */
    protected $searchData = [];

    /**
     * The filtered data after performing filteration.
     *
     * @var array
     */
    protected $result = [];

    /**
     * Aliases of filters.
     *
     * @var array
     */
    protected $aliases = [];

    /**
     * Factory instance of criteria.
     *
     * @var CriteriaFactoryContract
     */
    protected $factory = null;

    /**
     * Create a new Matcher instance.
     *
     * @param  CriteriaFactoryContract $factory
     * @param  array $params
     * @return void
     */
    public function __construct(CriteriaFactoryContract $factory)
    {
        $this->factory = $factory;
    }
    
    /**
     * Retrieve the result data.
     *
     * @return array
     */
    public function get()
    {
        return $this->result;
    }
    
    /**
     * Set the result data.
     *
     * @param  array  $data
     * @return void
     */
    public function setResult($data)
    {
        $this->result = $data;
    }

    /**
     * Retrieve the filters.
     *
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Retrieve search data.
     *
     * @return array
     */
    public function getSearchData()
    {
        return $this->searchData;
    }

    /**
     * Set the search data.
     *
     * @param  array  $data
     * @return $this
     */
    public function setSearchData($data)
    {
        $this->searchData = $data;
        return $this;
    }

    /**
     * Set the filters data.
     *
     * @param  array  $data
     * @return $this
     */
    public function setFilters($data)
    {
        $this->filters = $data;
        return $this;
    }
    
    /**
     * Retrieve alises.
     *
     * @return array
     */
    public function getAliases()
    {   
        return $this->aliases;
    }

    /**
     * Check for any alises of filter data before perform match with the search data.
     *
     * @param  string  $key
     * @param  array   $record
     * @return string
     */
    public function checkAlias($key, $record)
    {   
        $newKey = NULL;
        $aliases = $this->getAliases();
        if(sizeof($aliases) === 0 || !isset($aliases[$key])) {
            $newKey = $key;
        }else {
            $newKey = $aliases[$key];
        }
        return $newKey? $newKey: $key;
    }

    /**
     * Iterattes through each search data and match it with filters.
     *
     * @param  string  $searchDataKey
     * @param  array   $details
     * @return $this
     */
    public function match($searchDataKey = 'searchFields', $details = [])
    {
        $records = $this->getSearchData();
        foreach($records as $key => $record) {
            $record          = $this->transformData($record);
            $criteria        = $this->factory->createCriteria($record[$searchDataKey]);
            $matchedCriteria = $this->matchCriteria($criteria, $this->getFilters(), $details);
            $records[$key]   = $this->formattedResponse($records[$key], $matchedCriteria);
        }
        $this->setResult($records);
        return $this;
    }

    /**
     * Check for matching criteria with each search filter and return criteria response.
     *
     * @param  array    $record
     * @param  array    $searchFilters
     * @param  array    $details
     * @return object
     */
    public function matchCriteria($record, $searchFilters = [], $details = [])
    {
        $scoreResponse = [];
        foreach($searchFilters as $key => $filter) {
            $key = $this->checkAlias($key, $record);
            if(isset($record[$key])) {
                if($criteria = $record[$key]->criteria($filter, $details)) {
                    $scoreResponse[] = [
                        'criteria'   => $record[$key],
                        'is_matched' => $criteria
                    ];
                }
            }else {
                $scoreResponse[] = [
                    'criteria'   => NULL,
                    'is_matched' => false,
                ];
            }
        }
        return $scoreResponse;
    }

    /**
     * Convert the result into specific format.
     * 
     * @param  array $data
     * @param  array $matchedCriteria
     * @return mixed
     */
    public function formattedResponse($data, $matchedCriteria)
    {
        return $data;
    }

    /**
     * Tranform the data before passing it to the matching algorithm.
     * 
     * @param  array $data
     * @return array
     */
    public function transformData($data)
    {
        return $data;
    }
}
