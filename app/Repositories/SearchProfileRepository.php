<?php

namespace App\Repositories;

use App\Repositories\AbstractRepository;
use App\Models\SearchProfile;

/**
 * @author Syed Muhammad Ali Kamal
 * Created On 10 March, 2022
 */
class SearchProfileRepository extends AbstractRepository
{
    /**
     * This will hold the instance of SearchProfileRepository Class.
     * 
     * @var object
     */
    public $model;

    /**
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: campaign-1
     *
     * @var string
     **/
    protected $_cacheKey = 'search-profile';
    protected $_cacheTotalKey = 'total-search-profile';

    public function __construct(SearchProfile $model)
    {
        $this->model = $model;
        $this->builder = $model;
    }

    /**
     * This method will fetch all exsiting models
     * and will return output back to the controller
     *
     * @param  bool     pagination
     * @param  integer  $perPage
     * @param  array    $input
     * @return object 
     */
    public function findByAll($pagination = false, $perPage = 10, array $data = [] )
    {
        $this->builder = $this->model;
        if(!empty($data['order_by_created_at'])) {
            $this->builder = $this->model->orderBy('created_at', $data['order_by_created_at']);
        }
        if(!empty($data['filter_by_property_type'])) {
            $this->builder = $this->model->where('propertyType', (int)$data['filter_by_property_type']);
        }
        return parent::findByAll($pagination, $perPage, $data);
    }

    /**
     * This method will fetch a chunk of data from the table
     * and will return output back to the controller
     *
     * @param  integer  $chunk
     * @param  integer  $offset
     * @param  array    $where
     * @return array 
     */
    public function getByChunk($chunk = 10, $offset = 0, $where = [])
    {
        $this->builder = $this->model;
        foreach($where as $key => $value) {
            $this->builder = $this->builder->where($key, $value);
        }
        return $this->builder->skip($offset)->take($chunk)->get()->toArray();
    }

}
