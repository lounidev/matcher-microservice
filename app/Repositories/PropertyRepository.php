<?php

namespace App\Repositories;

use App\Repositories\AbstractRepository;
use App\Models\Property;

/**
 * @author Syed Muhammad Ali Kamal
 * Created On 10 March, 2022
 */
class PropertyRepository extends AbstractRepository
{
    /**
     * This will hold the instance of PropertyRepository Class.
     * 
     * @var object
     */
    public $model;

    /**
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: campaign-1
     *
     * @var string
     **/
    protected $_cacheKey = 'property';
    protected $_cacheTotalKey = 'total-property';

    /**
     * Create a new Matcher Repository instance.
     *
     * @param  object $model
     * @return void
     */
    public function __construct(Property $model)
    {
        $this->model = $model;
        $this->builder = $model;
    }

    /**
     * This method will fetch all exsiting models
     * and will return output back to the controller
     *
     * @param  bool     pagination
     * @param  integer  $perPage
     * @param  array    $input
     * @return object 
     */
    public function findByAll($pagination = false, $perPage = 10, array $data = [] )
    {
        $this->builder = $this->model;

        if(!empty($data['order_by_created_at'])) {
            $this->builder = $this->model->orderBy('created_at', $data['order_by_created_at']);
        }

        return parent::findByAll($pagination, $perPage, $data);
    }

    /**
     * This method will fetch a record from the table by property type attribute
     * and will return output back to the controller
     *
     * @param  integer  $id
     * @return array 
     */
    public function getFieldsByPropertyTypeId($id) 
    {
        $property = $this->findByAttribute('propertyType', $id);
        return !empty($property)? $property->fields: [];
    }

}
