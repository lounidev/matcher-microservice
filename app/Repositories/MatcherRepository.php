<?php

namespace App\Repositories;

use App\Repositories\AbstractRepository;
use App\Models\SearchProfileScore;
use Illuminate\Support\Facades\Schema;
use App\Services\CustomerProfileMatcher;

/**
 * @author Syed Muhammad Ali Kamal
 * Created On 10 March, 2022
 */
class MatcherRepository extends AbstractRepository
{
    /**
     * This will hold the instance of MatcherRepository Class.
     * 
     * @var object
     */
    public $model;

    /**
     * This will hold the instance of CustomerProfileMatcher Class.
     * 
     * @var object
     */
    public $customerProfileMatcher;

    /**
     * Prefix for holding search profile score dynamic mongodb table (collecion).
     * 
     * @var string
     */
    const BASE_TABLE_NAME = 'search_profile_score_';

    /**
     * Chunk size for chunking search profile records.
     * 
     * @var integer
     */
    const CHUNK_SIZE = 10000;

    /**
     * Default deviation applied for finding loose match count.
     * 
     * @var integer
     */
    const DEVIATION = 25;

    /**
     * This is the prefix of the cache key to which the
     * App\Data\Repositories data will be stored
     * App\Data\Repositories Auto incremented Id will be append to it
     *
     * Example: campaign-1
     *
     * @var string
     **/
    protected $_cacheKey = 'matcher';
    protected $_cacheTotalKey = 'total-matcher';

    /**
     * Create a new Matcher Repository instance.
     *
     * @param  object $model
     * @param  object $customerProfileMatcher
     * @return void
     */
    public function __construct(SearchProfileScore $model, CustomerProfileMatcher $customerProfileMatcher)
    {
        $this->model = $model;
        $this->customerProfileMatcher = $customerProfileMatcher;
    }

    /**
     * This method will fetch all exsiting models
     * and will return output back to the controller
     *
     * @param  bool     pagination
     * @param  integer  $perPage
     * @param  array    $input
     * @return object 
     */
    public function findByAll($pagination = false, $perPage = 10, array $data = [] )
    {
        if(empty($data['id'])){
            return null;
        }
        $this->builder = $this->model->setTable(self::BASE_TABLE_NAME.$data['id']);
        if(!empty($data['order_by_score'])) {
            $this->builder = $this->builder->orderBy('score', $data['order_by_score']);
        }
        if(!empty($data['order_by_strict_matches_count'])) {
            $this->builder = $this->builder->orderBy('strictMatchesCount', $data['order_by_strict_matches_count']);
        }
        return parent::findByAll($pagination, $perPage, $data);
    }

    /**
     * This method will drop the mongodb collection
     *
     * @param  string  $tableName
     * @return void 
     */
    public function dropTable($tableName) 
    {
        Schema::connection('mongodb')->dropIfExists($tableName);
    }

    /**
     * This method will bulk insert the records in to the table
     *
     * @param  string  $tableName
     * @param  array   $data
     * @return void 
     */
    public function insert($tableName, $data) 
    {
        if(!empty($data)) {
            $this->model->setTable($tableName)->insert($data);
        }
    }

    /**
     * This method chunks the search profile data
     * and calculate the scores using matchScore methods
     * and inserts the data into the respective searcg profile score table mongodb (collection)
     * 
     * @param  integer  $id
     * @param  integer  $deviation
     * @return bool 
     */ 
    public function matchAndInsert($id, $deviation = 25) 
    {
        $tableName = self::BASE_TABLE_NAME.$id;
        $this->dropTable($tableName);

        $fields = app(PropertyRepository::class)->getFieldsByPropertyTypeId($id);

        if(!empty($fields)){
            $offset = 0;
            while(true) {
                $searchProfiles = app(SearchProfileRepository::class)->getByChunk(self::CHUNK_SIZE, $offset, [ 'propertyType' => $id ]);
                if(sizeof($searchProfiles) == 0) break;
                $offset += self::CHUNK_SIZE;
                $matchScore = $this->matchScore($fields, $searchProfiles, $deviation);
                $this->insert($tableName, $matchScore);
            }
            return true;
        }else {
            return false;
        }
    }

    /**
     * This method chunks the search profile data
     * and calculate the scores using matchScore methods
     * and return the data to the controller
     * 
     * @param  integer  $id
     * @param  integer  $deviation
     * @return bool 
     */ 
    public function match($id, $deviation = 25) 
    {
        $fields = app(PropertyRepository::class)->getFieldsByPropertyTypeId($id);
        
        if(!empty($fields)){
            $offset = 0;
            $matchData = [];
            while(true) {
                $searchProfiles = app(SearchProfileRepository::class)->getByChunk(self::CHUNK_SIZE, $offset, [ 'propertyType' => $id ]);
                if(sizeof($searchProfiles) == 0) break;
                $offset += self::CHUNK_SIZE;
                $tmp = $this->matchScore($fields, $searchProfiles, $deviation);
                $matchData = array_merge_recursive($matchData, $tmp);
            }
            return $matchData;
        }else {
            return [];
        }
    }

    /**
     * This method utilizes the Matcher Service
     * and calculate the scores for every search profiles
     * 
     * @param  array    $fields
     * @param  array    $searchProfiles
     * @param  integer  $deviation
     * @return array 
     */ 
    public function matchScore($fields, $searchProfiles, $deviation) 
    {
        return $this->customerProfileMatcher
                    ->setFilters($fields)
                    ->setSearchData($searchProfiles)
                    ->match('searchFields', ['deviation' => $deviation])
                    ->orderBy('score')
                    ->get();  
    }
}
