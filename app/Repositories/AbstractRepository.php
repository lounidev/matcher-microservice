<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Cache;

use App\Contracts\RepositoryContract;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

/**
 * @author Syed Muhammad Ali Kamal
 * Created On 10 March, 2022
 */
abstract class AbstractRepository implements RepositoryContract 
{
    /**
     * The eloquent model instance of the associated the repository.
     * 
     * @var object
     */
    protected $builder;

    /**
     * This method will create a new model
     * and will return output back to the controller
     *
     * @param  array  $data
     * @return mixed
     */
    public function create(array $data = []) {

        $newInstance = $this->model->newInstance();
        foreach ($data as $column => $value) {
            $newInstance->{$column} = $value;
        }
        $newInstance->created_at = Carbon::now();
        $newInstance->updated_at = Carbon::now();
        if ($newInstance->save()) {
            $this->cache()::forget($this->_cacheTotalKey);
            return $this->findById($newInstance->id, true);
        } else {
            return false;
        }
    }

    /**
     * This method will fetch single model
     * and will return output back to the controller
     *
     * @param  integer  $id
     * @param  bool     $refresh
     * @param  bool     $details
     * @param  bool     $encode
     * @return mixed
     */
    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = $this->cache()::get($this->_cacheKey.$id);
        if ($data == NULL || $refresh == true) {
            $query = $this->model->find($id);
            if ($query != NULL) {
                $data = new \stdClass;
                foreach ($query->getAttributes() as $column => $value) {
                    $data->{$column} = $value;
                }
                $this->cache()::forever($this->_cacheKey.$id, $data);
            } else {
                return null;
            }
        }
        return $data;
    }

    /**
     * This method will fetch single model by attribute
     * and will return output back to the controller
     *
     * @param  string          $attribute
     * @param  integer|string  $value
     * @param  bool            $refresh
     * @param  bool            $details
     * @param  bool            $encode
     * @return Model
     */
    public function findByAttribute($attribute, $value, $refresh = false, $details = false, $encode = true) {
        $model = $this->model->newInstance()
                        ->where($attribute, '=', $value)->first(['id']);

        if ($model != NULL) {
            $model = $this->findById($model->id, $refresh, $details, $encode);
        }
        return $model;
    }

    /**
     * This method will fetch random model
     * and will return output back to the controller
     *
     * @param  bool  $refresh
     * @param  bool  $details
     * @param  bool  $encode
     * @return Model
     */
    public function findRandom($refresh = false, $details = false, $encode = true) {
        $model = $this->model->newInstance()
                        ->inRandomOrder()->first();

        if ($model != NULL) {
            $model = $this->findById($model->id, $refresh, $details, $encode);
        }
        return $model;
    }

    /**
     * This method will fetch all exsiting models
     * and will return output back to the controller
     *
     * @param  bool     $pagination
     * @param  integer  $perPage
     * @param  array    $input
     * @return object 
     */
    public function findByAll($pagination = false, $perPage = 10, array $input = [] ) {
        $ids = $this->builder;

        if ($pagination == true) {
            $ids = $ids->paginate($perPage);
            $models = $ids->items();
        } else {
            if($this->model->getConnectionName() == "mongodb") {
                $models = $ids->get();
            }else {
                $sql = $ids->toSql();
                $binds = $ids->getBindings();
                $models = DB::select($sql, $binds);
            }
        }
        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id, !empty($input['refresh']), !empty($input['details']), !empty($input['encode']));
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        }

        if ($pagination == true) {
            $data['pagination'] = [];
            $data['pagination']['total'] = $ids->total();
            $data['pagination']['current'] = $ids->currentPage();
            $data['pagination']['first'] = 1;
            $data['pagination']['last'] = $ids->lastPage();
            $data['pagination']['from'] = $ids->firstItem();
            $data['pagination']['to'] = $ids->lastItem();
            if($ids->hasMorePages()) {
                if ( $ids->currentPage() == 1) {
                    $data['pagination']['previous'] = -1;
                } else {
                    $data['pagination']['previous'] = $ids->currentPage()-1;
                }
                $data['pagination']['next'] = $ids->currentPage()+1;
            } else {
                $data['pagination']['previous'] = $ids->currentPage()-1;
                $data['pagination']['next'] =  $ids->lastPage();
            }
            if ($ids->lastPage() > 1) {
                $data['pagination']['pages'] = range(1,$ids->lastPage());
            } else {
                $data['pagination']['pages'] = [1];
            }
        }
        return $data;
    }

    /**
     * This method will update an existing model
     * and will return output back to the controller
     *
     * @param  array  $data
     * @return object 
     */
    public function update(array $data = []) {
        $model = $this->model->find($data['id']);
        if ($model != NULL) {
            foreach ($data as $column => $value) {
                $model->{$column} = $value;
            }
            $model->updated_at = Carbon::now();

            if ($model->save()) {

                return $this->findById($data['id'], true);
            }
            return false;
        }
        return NULL;
    }

    /**
     * This method will remove model
     * and will return output back to the controller
     *
     * @param  integer $id
     * @return bool 
     */
    public function deleteById($id) {
        $model = $this->model->find($id);
        if($model != NULL) {
            $this->cache()::forget($this->_cacheKey.$id);
            $this->cache()::forget($this->_cacheTotalKey);
            return $model->delete();
        }
        return false;
    }

    /**
     * This method will fetch total models
     * and will return output back to the controller
     *
     * @param  bool $refresh
     * @return integer 
     */
    public function findTotal($refresh = false) {

        $total = $this->cache()::get($this->_cacheTotalKey);
        if ($total == NULL || $refresh == true) {
            $total = $this->model->count();
            $this->cache()::forever($this->_cacheTotalKey, $total);
        }
        return $total;
    }

    public function getTranslationJson($data) {
        $language = app('language');
        $data = json_decode($data) ?: new \stdClass;

        if (isset($data->{$language})) {
            return $data->{$language};
        }
        return $data;
    }

    /**
     * This method will retrive cache according to repository
     *
     * @return Cache
     */
    protected function cache() {
        return Cache::class;
    }

    /**
     * This method will flush cache according to repo
     *
     * @return void
     */
    public function flush() {
        return $this->cache()::flush();
    }
}
