<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Factories\CriteriaFactory;
use App\Services\Matcher;
use App\Services\CustomerProfileMatcher;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CustomerProfileMatcher::class, function ($app) {
            return new CustomerProfileMatcher($app->makeWith(CriteriaFactory::class));
        });
    }
}
