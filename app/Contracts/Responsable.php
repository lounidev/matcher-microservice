<?php

namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface Responsable
{
    /**
     * Convert the result into specific format.
     * 
     * @param  array $data
     * @param  array $matchedCriteria
     * @return mixed
     */
    public function formattedResponse($data, $matchedCriteria);
}
