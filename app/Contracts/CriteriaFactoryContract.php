<?php

namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface CriteriaFactoryContract
{
    /**
     * Creates elements of each data in array into a criteria instance by using providers
     * defined in the criteria config.
     *
     * @param  array $data
     * @return array
     */
    public function createCriteria(array $data = []): array;
}
