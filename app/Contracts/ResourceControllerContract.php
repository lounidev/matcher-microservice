<?php

namespace App\Contracts;

use Illuminate\Http\Request;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface ResourceControllerContract 
{
    /**
     * This method will validate the request and fetch all the records 
     * with or without pagination and will return json output back to the client
     *
     * @param  object  $request
     * @return json
     */
    public function index(Request $request);

    /**
     * This method will validate the request and fetch single record
     * based on provided id and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer  $id
     * @return json
     */
    public function show(Request $request, $id);

    /**
     * This method will validate the request and creates a single record
     * and will return json output back to the client
     *
     * @param  object  $request
     * @return json
     */
    public function store(Request $request);

    /**
     * This method will validate the request and updates a single record
     * based on the provided id and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer  $id
     * @return json
     */
    public function update(Request $request, $id);

    /**
     * This method will validate the request and deletes a single record
     * based on the provided id and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer  $id
     * @return json
     */
    public function destroy(Request $request, $id);

    /**
     * This method defines the validation rules for validating the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function rules($methodName = '');

    /**
     * This method filters the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function input($methodName = '');

    /**
     * This method ovverrides the default error message bag of validation errors
     *
     * @param  string $value
     * @return array
     */
    public function messages($value = '');

    /**
     * This method defined the success response messages for the resource methods
     *
     * @param  string $value
     * @return array
     */
    public function responseMessages($value = '');
}
