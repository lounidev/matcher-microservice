<?php

namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface Serializable
{
    /**
     * iterates over each record and call a callback function.
     * 
     * @param  function  $fn
     * @return bool
     */
    public function serialize($fn);
}
