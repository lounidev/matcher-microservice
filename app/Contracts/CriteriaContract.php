<?php

namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface CriteriaContract
{
    /**
     * Retrieve the criteria value.
     *
     * @return mixed
     */
    public function getCriteriaValue();

    /**
     * Checks whether this criteria is applicable for criteria value 
     * by returning true / false.
     * 
     * @param  mixed $value
     * @return bool
     */
    public function applicable(): bool;

    /**
     * Set the criteria value.
     *
     * @param mixed $criteria_value
     * @return $this
     */
    public function setCriteriaValue($criteria_value);
    
    /**
     * Perform match based on range value.
     * 
     * @param  string|integer $value
     * @return bool
     */
    public function criteria($value);
}
