<?php
namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface RepositoryContract 
{
    /**
     * This method will fetch single model
     * and will return output back to the controller
     *
     * @param  integer  $id
     * @param  bool     $refresh
     * @param  bool     $details
     * @param  bool     $encode
     * @return mixed
     */
    public function findById($id, $refresh = false);

    /**
     * This method will fetch all exsiting models
     * and will return output back to the controller
     *
     * @param  bool     $pagination
     * @param  integer  $perPage
     * @param  array    $input
     * @return object 
     */
    public function findByAll($pagination = false, $perPage = 10);

    /**
     * This method will create a new model
     * and will return output back to the controller
     *
     * @param  array  $data
     * @return mixed
     */
    public function create(array $data = []);

    /**
     * This method will update an existing model
     * and will return output back to the controller
     *
     * @param  array  $data
     * @return object 
     */
    public function update(array $data = []);

    /**
     * This method will remove model
     * and will return output back to the controller
     *
     * @param  integer $id
     * @return bool 
     */
    public function deleteById($id);

}
