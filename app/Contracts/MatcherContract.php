<?php

namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface MatcherContract
{   
    /**
     * Retrieve the result data.
     *
     * @return array
     */
    public function get();
    
    /**
     * Set the result data.
     *
     * @param  array  $data
     * @return void
     */
    public function setResult($data);

    /**
     * Retrieve the filters.
     *
     * @return array
     */
    public function getFilters();

    /**
     * Retrieve search data.
     *
     * @return array
     */
    public function getSearchData();

    /**
     * Set the search data.
     *
     * @param  array  $data
     * @return $this
     */
    public function setSearchData($data);

    /**
     * Set the filters data.
     *
     * @param  array  $data
     * @return $this
     */
    public function setFilters($data);
    
    /**
     * Retrieve alises.
     *
     * @return array
     */
    public function getAliases();

    /**
     * Check for any alises of filter data before perform match with the search data.
     *
     * @param  string  $key
     * @param  array   $record
     * @return string
     */
    public function checkAlias($key, $record);

    /**
     * Iterattes through each search data and match it with filters.
     *
     * @param  string  $searchDataKey
     * @param  array   $details
     * @return $this
     */
    public function match($searchDataKey = 'searchFields', $details = []);

    /**
     * Check for matching criteria with each search filter and return criteria response.
     *
     * @param  array    $record
     * @param  array    $searchFilters
     * @param  array    $details
     * @return object
     */
    public function matchCriteria($record, $searchFilters = [], $details = []);

    /**
     * Tranform the data before passing it to the matching algorithm.
     * 
     * @param  array $data
     * @return array
     */
    public function transformData($data);
}
