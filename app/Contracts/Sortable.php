<?php

namespace App\Contracts;

/**
 * @author Syed Muhammad Ali Kamal
 */
interface Sortable
{
    /**
     * Sort the data based on attribute and sort flag.
     * 
     * @param  string  $key
     * @param  string  $order
     * @return $this
     */
    public function orderBy($key, $order = 'DESC');
}
