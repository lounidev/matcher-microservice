<?php

namespace App\Http\Controllers;


use App\Http\Controllers\AbstractResourceController;
use App\Repositories\PropertyRepository;
use Illuminate\Validation\Rule;

/**
 * @author Syed Muhammad Ali Kamal
 */
class PropertyController extends AbstractResourceController
{
    /**
     * Hold the reference of repository instance.
     * 
     * @var object
     */
    public $_repository;

    /**
     * Defined the per page data of paginated records.
     * 
     * @var integer
     */
    protected $per_page;

    /**
     * Create a new instance of AbstractResourceController Class.
     *
     * @param  object $repository
     * @return void
     */
    public function __construct(PropertyRepository $repository)
    {
        $this->_repository = $repository;
        $this->per_page = 10;
    }

     /**
     * This method defines the validation rules for validating the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function rules($value = '')
    {
        $rules = [];

        if($value == 'index') {
            $rules['pagination'] = 'nullable';
        }

        if($value == 'store') {
            $rules['name'] = 'required|string';
            $rules['address'] = 'required|string';
            $rules['propertyType'] = 'required|numeric|unique:mongodb.properties,propertyType';
            $rules['fields'] = 'required';
        }

        if($value == 'update') {
            $input = request()->only('id');
            $rules['id'] = 'required|exists:mongodb.properties,_id';
            $rules['name'] = 'required|string';
            $rules['address'] = 'required|string';
            $rules['propertyType'] = [
                'required',
                'numeric',
                Rule::unique('mongodb.properties')->ignore($input['id'], '_id')
            ];
            $rules['fields'] = 'required';
        }

        if($value == 'destroy') {
            $rules['id'] = 'required|exists:mongodb.properties,_id';
        }

        return $rules;

    }

    /**
     * This method filters the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function input($value = '')
    {
        $input = request()->only('pagination', 'order_by_created_at');

        if($value == 'store'){
            $input = request()->only('name', 'address', 'fields', 'propertyType');
        }
        
        if($value == 'update'){
            $input = request()->only('id', 'name', 'address', 'fields', 'propertyType');
        }
        
        if($value == 'destroy'){
            $input = request()->only('id');
        }

        return $input;
    }

}