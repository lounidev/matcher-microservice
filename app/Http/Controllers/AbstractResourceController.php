<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Contracts\ResourceControllerContract;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @author Syed Muhammad Ali Kamal
 */
abstract class AbstractResourceController extends Controller implements ResourceControllerContract
{
    /**
     * Hold the reference of repository instance.
     * 
     * @var object
     */
    public $_repository;
    
    /**
     * Defined the per page data of paginated records.
     * 
     * @var integer
     */
    protected $per_page;

    /**
     * Create a new instance of AbstractResourceController Class.
     *
     * @param  object $repository
     * @return void
     */
    public function __construct($repository)
    {
        $this->_repository = $repository;
        $this->per_page = 10;
    }

    /**
     * This method will validate the request and fetch all the records 
     * with or without pagination and will return json output back to the client
     *
     * @param  object  $request
     * @return json
     */
    public function index(Request $request)
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        $this->validate($request, $rules);
        
        $per_page = $this->per_page ? $this->per_page : config('app.per_page');
        $pagination = !empty($input['pagination']) ? $input['pagination'] : false; 
        $data = $this->_repository->findByAll($pagination, $per_page, $input);

        $output = [
            'data' => $data['data'],
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => $this->responseMessages(__FUNCTION__),
        ];

        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * This method will validate the request and fetch single record
     * based on provided id and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer  $id
     * @return json
     */
    public function show(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);
        $data = $this->_repository->findById($input['id'], $refresh = false, $input, $encode = true);

        $output = [
            'data' => $data
        ];

        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * This method will validate the request and creates a single record
     * and will return json output back to the client
     *
     * @param  object  $request
     * @return json
     */
    public function store(Request $request)
    {   
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        $messages = $this->messages(__FUNCTION__);

        $this->validate($request, $rules, $messages);
        $data = $this->_repository->create($input);
        
        $output = [
            'data' => $data,
            'message' => $this->responseMessages(__FUNCTION__)
        ];
        
        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * This method will validate the request and updates a single record
     * based on the provided id and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer  $id
     * @return json
     */
    public function update(Request $request, $id)
    {   
        $request->request->add(['id' => $id]);
        $input = $this->input(__FUNCTION__);
        $rules = $this->rules(__FUNCTION__);
        
        $messages = $this->messages(__FUNCTION__);
        $this->validate($request, $rules, $messages);
        $data = $this->_repository->update($input);

        $output = [
            'data' => $data,
            'message' => $this->responseMessages(__FUNCTION__)
        ];

        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * This method will validate the request and deletes a single record
     * based on the provided id and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer  $id
     * @return json
     */
    public function destroy(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);

        $messages = $this->messages(__FUNCTION__);
        $this->validate($request, $rules, $messages);
        $data = $this->_repository->deleteById($input['id']);

        $output = [
            'data' => $data,
            'message' => $this->responseMessages(__FUNCTION__)
        ];

        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * This method defines the validation rules for validating the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function rules($methodName = '')
    {
        $rules = [];

        if($methodName === 'index') {
            $rules['pagination'] = 'nullable';
        }

        if($methodName === 'show') {
            $rules['id'] = 'required';
        }

        if($methodName === 'store') {
            $rules = [
                //
            ];
        }

        if($methodName === 'update') {
            $rules = [
                //
            ];
        }

        if($methodName === 'destroy') {
            $rules['id'] = 'required';
        }

        return $rules;
    }

    /**
     * This method filters the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function input($methodName = '')
    {
        $input = [];

        if($methodName === 'index') {
            $input = request()->only('pagination');
        }

        if($methodName === 'show') {
            $input = request()->only('id');
        }

        if($methodName === 'store') {
            $input = request()->all();
        }

        if($methodName === 'update') {
            $input = request()->only('id');
        }

        if($methodName === 'destroy') {
            $input = request()->only('id');
        }

        return $input;
    }

    /**
     * This method ovverrides the default error message bag of validation errors
     *
     * @param  string $value
     * @return array
     */
    public function messages($value = '')
    {
        $messages = [
            //
        ];
        return $messages;
    }

    /**
     * This method defined the success response messages for the resource methods
     *
     * @param  string $value
     * @return array
     */
    public function responseMessages($value = '')
    {
        $messages = [
            'index'   => 'Record retrieved successfully.',
            'store'   => 'Record created successfully.',
            'update'  => 'Record updated successfully.',
            'destroy' => 'Record deleted successfully.',
        ];
        
        return !empty($messages[$value]) ? $messages[$value] : 'Success.';
    }
}