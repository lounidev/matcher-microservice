<?php

namespace App\Http\Controllers;


use App\Http\Controllers\AbstractResourceController;
use App\Repositories\SearchProfileRepository;

/**
 * @author Syed Muhammad Ali Kamal
 */
class SearchProfileController extends AbstractResourceController
{        
    /**
    * Hold the reference of repository instance.
    * 
    * @var object
    */
   public $_repository;

   /**
    * Defined the per page data of paginated records.
    * 
    * @var integer
    */
   protected $per_page;

   /**
    * Create a new instance of AbstractResourceController Class.
    *
    * @param  object $repository
    * @return void
    */
    public function __construct(SearchProfileRepository $repository)
    {
        $this->_repository = $repository;
        $this->per_page = 100;
    }

     /**
     * This method defines the validation rules for validating the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function rules($value = '')
    {
        $rules = [];

        if($value == 'index') {
            $rules['pagination'] = 'nullable';
        }

        if($value == 'store') {
            $rules['name'] = 'required|string';
            $rules['propertyType'] = 'required|numeric';
            $rules['searchFields'] = 'required';
        }

        if($value == 'update') {
            $rules['id'] = 'required|exists:mongodb.search_profiles,_id';
            $rules['name'] = 'required|string';
            $rules['propertyType'] = 'required|numeric';
            $rules['searchFields'] = 'required';
        }

        if($value == 'destroy') {
            $rules['id'] = 'required|exists:mongodb.search_profiles,_id';
        }

        return $rules;

    }

    /**
     * This method filters the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function input($value = '')
    {
        $input = request()->only('pagination', 'order_by_created_at', 'filter_by_property_type');

        if($value == 'store'){
            $input = request()->only('name', 'searchFields', 'propertyType');
        }
        
        if($value == 'update'){
            $input = request()->only('id', 'name', 'searchFields', 'propertyType');
        }
        
        if($value == 'destroy'){
            $input = request()->only('id');
        }

        return $input;
    }
}