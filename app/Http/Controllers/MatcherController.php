<?php

namespace App\Http\Controllers;


use App\Http\Controllers\AbstractResourceController;
use App\Repositories\MatcherRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @author Syed Muhammad Ali Kamal
 */
class MatcherController extends AbstractResourceController
{
    /**
     * Hold the reference of repository instance.
     * 
     * @var object
     */
    public $_repository;

    /**
     * Defined the per page data of paginated records.
     * 
     * @var integer
     */
    protected $per_page;

    /**
     * Create a new instance of AbstractResourceController Class.
     *
     * @param  object $repository
     * @return void
     */
    public function __construct(MatcherRepository $repository)
    {
        $this->_repository = $repository;
        $this->per_page = 100;
    }

    /**
     * This method defines the validation rules for validating the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function rules($methodName = '')
    {
        $rules = [];

        if($methodName == 'index') {
            $rules['pagination'] = 'nullable';
        }
        if($methodName == 'matchAndInsert') {
            $rules['id'] = 'required';
        }
        if($methodName == 'match') {
            $rules['id'] = 'required';
        }

        return $rules;
    }

    /**
     * This method filters the request data
     *
     * @param  string $methodName
     * @return array
     */
    public function input($methodName = '')
    {
        $input = request()->only('id', 'pagination', 'order_by_score', 'order_by_strict_matches_count');

        if($methodName == 'matchAndInsert') {
            $input = request()->only('id', 'deviation');
        }
        if($methodName == 'match') {
            $input = request()->only('id', 'deviation');
        }

        return $input;
    }

    /**
     * This method will validate the request and calculates all search profile score
     * and store that scores in the database 
     * and will return json output back to the client
     *
     * @param  object  $request
     * @return json
     */
    public function matchAndInsert(Request $request) 
    {
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);
        $data = $this->_repository->matchAndInsert($input['id'], $input['deviation']);

        $output = [
            'data' => $data
        ];

        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * This method will validate the request and calculates all search profile score
     * and will return json output back to the client
     *
     * @param  object  $request
     * @param  integer $id
     * @return json
     */
    public function match(Request $request, $id) 
    {
        $request->request->add(['id' => $id]);
        $rules = $this->rules(__FUNCTION__);
        $input = $this->input(__FUNCTION__);
        
        $this->validate($request, $rules);
        if(!isset($input['deviation'])) {
            $input['deviation'] = 25;
        }
        $data = $this->_repository->match((int)$input['id'], $input['deviation']);

        $output = [
            'data' => $data
        ];

        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }
}

