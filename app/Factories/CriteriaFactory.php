<?php

namespace App\Factories;

use App\Contracts\CriteriaFactoryContract;

/**
 * @author Syed Muhammad Ali Kamal
 */
class CriteriaFactory implements CriteriaFactoryContract
{
    /**
     * Create a new CriteriaFactory instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Creates elements of each data in array into a criteria instance by using providers
     * defined in the criteria config.
     *
     * @param  array $data
     * @return array
     */
    public function createCriteria(array $data = []): array
    {
        $config = config('criteria.provider');
        $criteria = array();
        foreach($data as $key => $val) {
            foreach($config as $criteriaClass) {
                $class = new $criteriaClass($val);
                if($class->applicable()) {
                    $criteria[$key] = $class;
                }
            }
        }
        return $criteria;
    }
}
