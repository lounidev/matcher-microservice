<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Criteria for Matching Algorithm
    |--------------------------------------------------------------------------
    |
    | Here are all the providers for you criteria factory service.
    | You can make your own criteria and add in this array of provider given below.
    | Criteria Factory will iterates through each provider in the order in which criteria 
    | are added.
    |
    */

    'provider' => [
        \App\Services\ArrayWithDeviationCriteria::class,
        \App\Services\NumericCriteria::class,
        \App\Services\BooleanCriteria::class,
    ],

];
